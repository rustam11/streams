import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';

import Modal from '../Modal';
import history from '../../history';
import {deleteStream, fetchStream} from '../../actions';

class StreamDelete extends React.Component {
    componentDidMount() {
        this.props.fetchStream(this.props.match.params.id);
    };

    renderActions() {
        const {id} = this.props.match.params;
        return (
            <>
                <button
                    className="ui button negative"
                    onClick={() => this.props.deleteStream(id)}
                >
                    Delete
                </button>
                <Link className="ui button" to={'/'}>
                    Cancel
                </Link>
            </>
        );
    };

    renderContent() {
        let content = 'Are you sure you want to delete this stream?';
        if (this.props.stream) {
            content = `Are you sure you want to delete 
            ${this.props.stream.title} stream?`;
        }
        return content;
    }

    render() {
        return (
            <Modal
                title="Delete Stream"
                content={this.renderContent()}
                actions={this.renderActions()}
                onDismiss={() => history.push('/')}
            />
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {stream: state.streams[ownProps.match.params.id]};
};

export default connect(mapStateToProps, {fetchStream, deleteStream})(StreamDelete);
